"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import os
import flask
from flask import redirect, url_for, request, render_template
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient('mongodb://mongo:27017/docker-node-mongo')
db = client.tododb
collection = db.controles

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')

@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404

###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    dateWithTime = request.args.get('dateWithTime', 997, type=str) #ADDED BY SIMON WARD
    totalDist = request.args.get('totalDist', 998, type=int) #ADDED BY SIMON WARD
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    if(km > totalDist):
        return flask.jsonify(result="distance too long")
    open_time = acp_times.open_time(km, totalDist, dateWithTime) 
    close_time = acp_times.close_time(km, totalDist, dateWithTime) 
    results = {"open": open_time, "close": close_time}
    return flask.jsonify(result=results)

@app.route("/_submit_data", methods=['GET', 'POST'])
def _submit_data():
    openValues = request.args.get('openValues', type=str)
    closeValues = request.args.get('closeValues', type=str)
    openValues = str(openValues)
    closeValues = str(closeValues)
    tFrame = "Open: " + openValues + " - Close: " + closeValues
    timeframe = {'time' : tFrame}
    collection.insert_one(timeframe)
    results = ("inserted " + tFrame)
    return flask.jsonify(result=results)

@app.route("/_display_data", methods=['GET','POST'])
def _display_data():
    dbList = []
    for time in collection.find():
        dbList.append(time)
    if(len(dbList) <= 0):
        flask.redirect(url_for(page_not_found("No controle times have been entered!")))
    return render_template('todo.html', items=dbList)

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
